﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Assignment1
{
    class HSB
    {
       public HSB() // init
        {

        }

        public Color HSBtoRGB(float hue, float sat, float bri) //convert to RGB
        {
            int r = 0, g = 0, b = 0;
            if (sat == 0){
                r = g = b = (int)(bri * 255.0f + 0.5f);
            }
            else
            {
                float h = (hue - (float)Math.Floor(hue)) * 6.0f;
                float f = h - (float)Math.Floor(h);
                float p = bri * (1.0f - sat);
                float q = bri * (1.0f - sat * f);
                float t = bri * (1.0f - (sat * (1.0f - f)));//initliazies the variables
               
                
                if((int)h==0){     //using the if statments, h=1-5 to choose
               r = (int)(bri * 255.0f + 0.5f);
                        g = (int)(t * 255.0f + 0.5f);
                        b = (int)(p * 255.0f + 0.5f);
                   
               }
                    else if((int)h==1){
                        r = (int)(q * 255.0f + 0.5f);
                        g = (int)(bri * 255.0f + 0.5f);
                        b = (int)(p * 255.0f + 0.5f);
                    }
                   else if((int)h==2){
                        r = (int)(p * 255.0f + 0.5f);
                        g = (int)(bri * 255.0f + 0.5f);
                        b = (int)(t * 255.0f + 0.5f);
                   }
                   else if((int)h==3){
                        r = (int)(p * 255.0f + 0.5f);
                        g = (int)(q * 255.0f + 0.5f);
                        b = (int)(bri * 255.0f + 0.5f);
                   }
                else if((int)h==4){
                        r = (int)(t * 255.0f + 0.5f);
                        g = (int)(p * 255.0f + 0.5f);
                        b = (int)(bri * 255.0f + 0.5f);
                }
               else if ((int)h == 5){
                        r = (int)(bri * 255.0f + 0.5f);
                        g = (int)(p * 255.0f + 0.5f);
                        b = (int)(q * 255.0f + 0.5f);
               }
            }

            return Color.FromArgb(Convert.ToByte(255), Convert.ToByte(r), Convert.ToByte(g), Convert.ToByte(b));//convert the rgb to the byte
        }
    }
}
