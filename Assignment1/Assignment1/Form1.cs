using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Assignment1
{
    public partial class Form1 : Form //set the values!!!!!
    {
        private readonly int MAX = 256;      // max iterations
        private readonly double SX = -2.025; // start value real
        private readonly double SY = -1.125; // start value imaginary
        private readonly double EX = 0.6;    // end value real
        private readonly double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, finished, pP;
        private static double xy;
        private Graphics g1;
        private Bitmap picture = new Bitmap(640, 480);
        private Pen dra = new Pen(Color.Red);
        private HSB HSBcol = new HSB();
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)//set the from load
        {
            x1 = pictureBox1.Width;
            y1 = pictureBox1.Height;
            xy = (double)x1 / (double)y1;
            g1 = Graphics.FromImage(picture);
            init();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            m();
            pictureBox1.Image = picture;  

        }

        private void init() // init the values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;

        }

        private void m() // total points
        {
            int x, y;
            float h, b = 0.0f;
            double alt = 0.0;

            action = false;
            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++){
                    h = pc(xstart + xzoom * (double)x, ystart + yzoom * (double)y); //color value
                    if (h != alt){
                   b = 1.0f - h * h; // brightnes
                   dra.Color = HSBcol.HSBtoRGB(h, 0.8f, b);//Convert HSB to RGB and set the color
                   alt = h;
                    }
                    g1.DrawLine(dra, x, y, x + 1, y);
                }
            action = true;
        }

        private float pc(double xwert, double ywert) // set the color value
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int jj = 0;
            while ((jj < MAX) && (m < 4.0)){
                jj++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)jj / (float)MAX;
        }

        private void Status_Click(object sender, EventArgs e)
        {

        }



        public void iLovePaint(){
            pictureBox1.Image = picture;
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)//set the mouse down
        {
            pP = true;
            if (action){
                xs = e.X;
                ys = e.Y;
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)//set the mouse up actions
        {
            int z, w;

            if (action){
                xe = e.X;
                ye = e.Y;
                if (xs > xe){
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye){
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) init();
                else{
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                m();
                rectangle = false;
                iLovePaint();
            }
            pP = false;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)//set the mouse movie order
        {
            if (pP && action){
                xe = e.X;
                ye = e.Y;
                rectangle = true;
                Bitmap tpc = new Bitmap(picture);
                Graphics tg = Graphics.FromImage(tpc);
                dra.Color = Color.White;
                if (xs < xe)
                {
                    if (ys < ye) tg.DrawRectangle(dra, xs, ys, (xe - xs), (ye - ys));
                    else tg.DrawRectangle(dra, xs, ye, (xe - xs), (ys - ye));
                }
                else
                {
                    if (ys < ye) tg.DrawRectangle(dra, xe, ys, (xs - xe), (ye - ys));
                    else tg.DrawRectangle(dra, xe, ye, (xs - xe), (ys - ye));
                }

                pictureBox1.Image = tpc;

            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)//set the click the form
        {

        }
   

    }
}
